<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class ReportPertahunHuawei extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        
        $tahun = $this->db->query("SELECT DISTINCT SUBSTRING(bot_huawei.endtime, 1, 4) as year FROM bot_huawei")->result();
  
        $this->response($tahun, 200);
    }
}