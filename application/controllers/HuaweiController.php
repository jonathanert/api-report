<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class HuaweiController extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
      $regional = $this->get('regional');
  	
      if (isset($_GET['month'])) {
        $month = $_GET['month'];
      }

      if ($regional != '') {
	      $detail = $this->db->query('SELECT * FROM bot_huawei WHERE bot_huawei.regional = "'.str_replace("_", " ", $regional).'" AND bot_huawei.changeid = "REMEDY NOK" AND MONTH(bot_huawei.endtime) = '.$month.'')->result();
      }

      $this->response($detail, 200);
    }

    function index_post(){
        $posted = $this->input->post();
        $huawei = $this->input->post();
        foreach ($huawei as $post) {
          $data = array(
              'BTS_NAME'  => $posted['btsname'][$post],
              'USERNAME'  => $posted['username'][$post],
              'REGIONAL'  => $posted['regional'][$post],
              'CHANGE_ID' => $posted['changeid'][$post],
              'RESULT' 	  => $posted['result'][$post],
              'END_TIME'  => $posted['endtime'][$post]
          );

          if($insert){
              $this->response($data,200);
          } else{
              $this->response(array('status' => 'fail', 502));
          }

          $i++;
        }
    }
}