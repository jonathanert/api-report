<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class ZTEController extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function get_zte() {
        $btsname = $this->get('btsname');
        if ($btsname == '') {
            $kontak = $this->db->query("SELECT * FROM bot_zte WHERE bot_zte.btsname LIKE "%'.$btsname.'%"")->result();
        }
        $this->response($kontak, 200);
    }

    function post_zte(){
        $posted = $this->input->post();
        $zte = $this->input->post();
        foreach ($zte as $post) {
          $data = array(
              'BTS_NAME'  => $posted['btsname'][$post],
              'USERNAME'  => $posted['username'][$post],
              'REGIONAL'  => $posted['regional'][$post],
              'CHANGE_ID' => $posted['changeid'][$post],
              'RESULT' 	  => $posted['result'][$post],
              'END_TIME'  => $posted['endtime'][$post]
          );

          if($insert){
              $this->response($data,200);
          } else{
              $this->response(array('status' => 'fail', 502));
          }

          $i++;
        }
    }
}